package network.marble.bugreport;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import lombok.Getter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import network.marble.bugreport.commands.Report;
import network.marble.bugreport.config.Config;

import org.bukkit.plugin.java.JavaPlugin;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class BugReport
extends JavaPlugin
{
	@Getter private static BugReport instance;
	@Getter private static JedisPool pool;
	public static Config config;

	public void onEnable()
	{
		instance = this;

		config = retrieveConfig();
		if (config == null)
		{
			createConfig();
			config = retrieveConfig();
		}
		setupJedisPool();
		registerCommands();
	}

	private void setupJedisPool() {
		try
		{
			JedisPoolConfig poolcfg = new JedisPoolConfig();
			poolcfg.setMaxTotal(getServer().getMaxPlayers() + 1);
			pool = new JedisPool(new JedisPoolConfig(), config.getRedisHost(), config.getRedisPort());
		}
		catch (Exception e)
		{
			getLogger().severe("Failed to load at Redis setup.");
			e.printStackTrace();
			setEnabled(false);
		}
	}

	public void registerCommands() {
		getCommand("bugreport").setExecutor(new Report());
	}

	private Config retrieveConfig() {
		try(Reader reader = new FileReader("plugins/MRN-BugReport/config.json")){
			Config container = new Gson().fromJson(reader, Config.class);
			return container;
		} catch (Exception e){
			getLogger().info("Config does not exist... Creating");
		}
		return null;
	}

	private void createConfig(){
		Config builder = new Config(); 
		File directory = new File("plugins/MRN-BugReport");
		
		if (directory.exists() == false){
			getLogger().info("Creating Directory");
			directory.mkdir();
			getLogger().info("Created");
		}
		
		try(Writer writer = new BufferedWriter( new OutputStreamWriter( new FileOutputStream("plugins/MRN-BugReport/config.json" )))){
			Gson file = new GsonBuilder().create();
			file.toJson(builder, writer);
			getLogger().info("Config file has been created");
		} catch (Exception e){
			e.printStackTrace();
			this.setEnabled(false);
		}
	}
}
