package network.marble.bugreport.commands;

import java.util.logging.Logger;
import net.gpedro.integrations.slack.SlackApi;
import net.gpedro.integrations.slack.SlackMessage;
import network.marble.bugreport.BugReport;
import network.marble.bugreport.config.Config;
import network.marble.bugreport.utils.FontFormat;
import network.marble.datastoragemanager.database.DAOManager;
import network.marble.datastoragemanager.database.models.BugReports;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class Report
  implements CommandExecutor
{
  public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
  {
    if ((sender instanceof Player)) {
      try
      {
        Jedis jedis = BugReport.getPool().getResource();
        if (!jedis.exists("BugReport:" + ((Player)sender).getUniqueId()).booleanValue())
        {
          String reason = "";
          for (String s : args) {
            reason = reason + " " + s;
          }
          if (reason == "")
          {
            sender.sendMessage(FontFormat.translateString("&cYou must provide a reason"));
            return false;
          }
          jedis.setex("BugReport:" + ((Player)sender).getUniqueId(), 60 * BugReport.config.getReportFrequencyMinutes(), reason);
          
          BugReports b = new BugReports((Player)sender, reason);
          DAOManager.getBugReportDAO().save(b);
          
          SlackApi api = new SlackApi("https://hooks.slack.com/services/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");
          api.call(new SlackMessage(((Player)sender).getName() + " Submitted a report with reason: " + reason));
          
          sender.sendMessage(FontFormat.translateString("&aYour report has been created."));
          return true;
        }
        else
        {
          sender.sendMessage(FontFormat.translateString("&cYou can only create a bug report once every " + BugReport.config.getReportFrequencyMinutes() + " minutes. "));
          
          BugReport.getInstance().getLogger().info(jedis.ttl("BugReport:" + ((Player)sender).getUniqueId()).toString());
          return false;
        }
      }
      catch (Exception e)
      {
        sender.sendMessage(FontFormat.translateString("&cAn error has occured. Please try again later or contact an administrator."));
        e.printStackTrace();
        return false;
      }
    }
    sender.sendMessage(FontFormat.translateString("&cYou must be a player to execute this command!"));
    
    return false;
  }
}

