package network.marble.bugreport.config;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Config {

	private String redisHost = "loclhost";
	
	private String redisUser = "";
	
	private String redisPass = "";
	
	private int redisPort = 6379;
	
	private int reportFrequencyMinutes = 10;
	
}
